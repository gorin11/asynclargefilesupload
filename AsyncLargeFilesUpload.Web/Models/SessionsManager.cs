﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AsyncLargeFilesUpload.Web.Models
{
    public static class SessionsManager
    {
        static Dictionary<string, IList<string>> fileParts = new Dictionary<string, IList<string>>();

        public static void AddItem(string key, string value)
        {
            fileParts[key].Add(value);
        }

        public static bool CreateIfNotExists(string key)
        {
            bool result = false;
            if (!fileParts.Keys.Contains(key) ||fileParts[key] == null ||fileParts[key].Count == 0)
            {
                fileParts.Add(key, new List<string>());
                result = true;
            }
            return result;
        }

        public static IList<string> GetBlockListBySessionId(string sessionId)
        {
            return fileParts[sessionId];
        }
    }
}