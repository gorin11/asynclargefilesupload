﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AsyncLargeFilesUpload.Web.Models
{
    public class CompleteFileUploadRequest
    {
        public string FileName { get; set; }
        public string SessionId { get; set; }
    }
}