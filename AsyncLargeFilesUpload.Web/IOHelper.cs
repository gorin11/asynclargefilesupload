﻿using System.IO;
using System.Web;

namespace AsyncLargeFilesUpload.Web
{
	public static class IoHelper
	{
		public static void DeleteFilesByExtention(HttpContext context, string ext, string path)
		{
			var di = new DirectoryInfo(path);
			var fi = di.GetFiles("*." + ext);
			foreach (var f in fi)
				try
				{
					f.Attributes = FileAttributes.Normal;
					File.Delete(f.FullName);
				}
				catch
				{
					// ignored
				}

			//Directory.GetFiles(
			//	context.Server.MapPath(ConfigurationManager.AppSettings["PathsFilesUpload"].Split(',')[0]),
			//	"*." + path);
		}
	}
}