﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AsyncLargeFilesUpload.Web.Models;
using AsyncLargeFilesUpload;


namespace AsyncLargeFilesUpload.Web.Controllers
{
	public class UploadController : Controller
	{
		//
		// GET: /Upload/

		public ActionResult Index()
		{
			return View();
		}

		public JsonResult MergeBlocks(CompleteFileUploadRequest file)
		{
			var paths = ConfigurationManager.AppSettings["PathsFilesUpload"].Split(',');
			string[] files = Directory.GetFiles(Server.MapPath(paths[0]), "*." + file.SessionId);
			if (files.Any())
			{
				Array.Sort(files, StringComparer.InvariantCulture);
				if(Directory.Exists(Server.MapPath(paths[1])))
				{
					using (var outputStream = System.IO.File.Create(Server.MapPath(paths[1] + "/" + file.FileName)))
					{
						foreach (var inputFilePath in files)
							using (var inputStream = System.IO.File.OpenRead(inputFilePath))
							{
								inputStream.CopyTo(outputStream);
							}
					}
					
				}
			}
			IoHelper.DeleteFilesByExtention(System.Web.HttpContext.Current,file.SessionId, Server.MapPath(paths[0]));
			return Json("Ok");
		}
	}
}