﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using AsyncLargeFilesUpload.Web.Models;

namespace AsyncLargeFilesUpload.Web
{
	public class FileTransfer : IHttpHandler
	{
		private const string SessionIdKeyName = "sessionId";

		public void ProcessRequest(HttpContext context)
		{
			if (!context.Request.Params.AllKeys.Contains(SessionIdKeyName))
				throw new ArgumentNullException("Session id required");
			SessionsManager.CreateIfNotExists(context.Request[SessionIdKeyName]);
			context.Response.AddHeader("Pragma", "no-cache");
			context.Response.AddHeader("Cache-Control", "private, no-cache");
			UploadFile(context);
		}

		public bool IsReusable => false;

		private void UploadFile(HttpContext context)
		{
			var headers = context.Request.Headers;

			if (!string.IsNullOrEmpty(headers["X-File-Name"]))
				UploadPartialFile(Path.GetFileName(headers["X-File-Name"]), context);

			WriteResponse(context);
		}

		private void WriteResponse(HttpContext context)
		{
			context.Response.AddHeader("Vary", "Accept");
			try
			{
				if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
					context.Response.ContentType = "application/json";
				else
					context.Response.ContentType = "text/plain";
			}
			catch
			{
				context.Response.ContentType = "text/plain";
			}

			context.Response.Write(@"{ Result: 1, Message: ""Chunk Uploaded"" }");
		}

		private void UploadPartialFile(string fullName, HttpContext context)
		{
			if (context.Request.Files.Count != 1)
				throw new HttpRequestValidationException(
					"Attempt to upload chunked file containing more than one fragment per request");
			var inputStream = context.Request.Files[0].InputStream;
			byte[] fileBytes;
			using (var reader = new BinaryReader(inputStream))
			{
				fileBytes = reader.ReadBytes((int) inputStream.Length);
			}

			var blockId = DateTime.UtcNow.ToString("yyyy-MM-dd-HH-mm-ss-fff",CultureInfo.InvariantCulture);
			SessionsManager.AddItem(context.Request[SessionIdKeyName], blockId.ToString());

			var path = ConfigurationManager.AppSettings["PathsFilesUpload"].Split(',')[0];
			using (var fStream =
				File.Create(
					HttpContext.Current.Server.MapPath(path+ "/" + blockId + "." + context.Request[SessionIdKeyName])))
			{
				using (var stream = new MemoryStream(fileBytes, true))
				{
					stream.WriteTo(fStream);
				}
			}
		}
	}
}